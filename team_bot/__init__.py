import toml
from discord.ext import commands
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from team_bot.utils import Config, get_cogs

bot_config = Config()
client = commands.Bot(command_prefix=bot_config.PREFIX)

engine = create_engine("sqlite:///db.sqlite")
Session = sessionmaker(bind=engine)

Base = declarative_base()


@client.event
async def on_ready():
    """
    When client starts up display online message in CLI
    """

    print(f"'{client.user.name}' is online with the ID '{client.user.id}'!")

    print("Loading cogs..")
    for cog in get_cogs():
        client.load_extension(f"{__name__}.{cog}")
    print("Loaded cogs!")
