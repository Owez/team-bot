import os
import toml
from discord import Embed


class Config:
    """
    Config object containing basic bot infomation
    """

    def __init__(self):
        self.CONFIG_PATH = "config.toml"
        self.CONFIG = self._get_config()
        self.PREFIX = self.CONFIG["prefix"]
        self.TOKEN = self._get_env_var("TOKEN")
        self.TEAM_MANAGER = self.CONFIG["tourney_manager"]

    def _get_config(self):
        """
        Gets config from specified TOML file
        """

        return toml.load(open(self.CONFIG_PATH, "r"))

    def _get_env_var(self, var_name):
        """
        Frontend function to hook onto to get an enviroment
        variable of a specified name
        """

        return os.environ[var_name]


def get_cogs():
    """
    Gets all bot cogs from rfmp_comp_bot/cogs

    < Returns list of cog.x
    """

    out_list = []

    for cog_file in os.walk(os.path.dirname(os.path.abspath(__file__)) + "/cogs/"):
        for cog in cog_file[2]:
            if cog[-3:] == ".py":
                out_list.append(f"cogs.{cog[:-3]}")

    return out_list


def embed_generator(gen_text):
    """
    Discord embed generator, using a small templating system

    - gen_text: a dict following the following syntax:
        INGRESS SPEC
        {
            [BODY]: {
                [FIELD NAME]: [FIELD TEXT],
                [FIELD NAME]: [FIELD TEXT]
            },
            [LOGO (optional)]: [LOGO URL]
        }
    < Returns embed object from discord
    """

    embed = Embed()

    for field in gen_text["body"].items():
        embed.add_field(name=field[0], value=field[1])

    if "image" in gen_text:
        embed.set_image(url=gen_text["image"])

    return embed


def load_message(cog_name):
    print(f"\t✓ Loaded {cog_name} cog!")
