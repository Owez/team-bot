from discord.ext import commands
from team_bot.utils import embed_generator, load_message


class Ping(commands.Cog):
    """
    Debug ping command, testing the most simple get + response mechanism.
    """

    def __init__(self, client):
        self.client = client

    @commands.command(
        name="ping",
        description="Ping command, testing the most simple get + response mechanism.",
    )
    async def ping_command(self, ctx):
        RETURN_EMOTE = "🏓"

        await ctx.send(embed=embed_generator({"body": {"Ping": RETURN_EMOTE}}))


def setup(client):
    """
    Binds this cog to the client object
    """

    client.add_cog(Ping(client))
    load_message("Ping")
