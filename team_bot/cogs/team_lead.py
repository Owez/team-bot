import discord
import slugify
from discord.ext import commands
from team_bot import Session
from team_bot.models import TeamModel, dynamic_user_get
from team_bot.utils import embed_generator, load_message


class TeamLead(commands.Cog):
    """
    Contains commands for the team leader.
    """

    def __init__(self, client):
        self.session = Session()
        self.client = client

    async def _team_get(self, ctx, team_name):
        """
        Finds the team and returns various permissions errors or
        a 404 error if not a valid team.
        """

        found_team = (
            self.session.query(TeamModel)
            .filter_by(slug=slugify.slugify(team_name))
            .first()
        )

        if found_team is None:
            error_payload = {
                "body": {
                    "Team not found": f"Could not find the requested team named {team_name}!"
                }
            }
            await ctx.send(embed=embed_generator(error_payload))

            return
        elif found_team.owner != ctx.author.id:
            error_payload = {
                "body": {
                    "body": {
                        "Invalid permissions": "You are not the team leader and therefore cannot access this command!"
                    }
                }
            }
            await ctx.send(embed=embed_generator(error_payload))

            return

        return found_team

    async def _wait_for_pm(self, ctx, user, team_name):
        """
        Waits for a pm from a specific user with the content "yes"
        or initiates recursion until it finds
        """

        def accept_check(message):
            """
            Checks if user to pm sends "yes"
            """

            return user.id == message.author.id and message.guild is None

        accepted_invite = await self.client.wait_for(
            "message", timeout=120, check=accept_check
        )

        if accepted_invite.content.lower() == "yes":
            join_payload = {"body": {"Joining": f"You are now joining '{team_name}'.."}}

            await user.send(embed=embed_generator(join_payload))
            return True
        elif accepted_invite.content.lower() == "no":
            decline_payload = {
                "body": {"Declined": f"You have declined to join {team_name}."}
            }

            await user.send(embed=embed_generator(decline_payload))
            return False
        else:
            invalid_response_payload = {
                "body": {
                    "Invalid response": f"'{accepted_invite.content}' is not a valid answer! Please type `Yes` or `No`."
                }
            }
            await user.send(embed=embed_generator(invalid_response_payload))

            return self._wait_for_pm(ctx, user)

    @commands.command(
        name="invite",
        description="Sends a pm to the first mentioned user to join a team.",
    )
    async def invite_command(self, ctx, team_name, user: discord.Member):
        found_team = await self._team_get(ctx, team_name)
        new_member = dynamic_user_get(user.id, self.session)

        invite_response_payload = {
            "body": {
                "Inviting user": f"You have invited '{user.name}' to '{team_name}'!"
            }
        }
        await ctx.send(embed=embed_generator(invite_response_payload))

        invite_payload = {
            "body": {
                "Invited": f"You have been invited to '{team_name}'!",
                "Responding": "Please type `Yes` or `No` in the chat below!",
            }
        }
        await user.send(embed=embed_generator(invite_payload))

        accepted_invite = await self._wait_for_pm(ctx, user, team_name)

        if accepted_invite:
            found_team.users.append(new_member)
            self.session.commit()

            accepted_payload = {
                "body": {
                    "Added new user successfully": (
                        f"Added <@{user.id}> to '{found_team.name}' successfully!"
                    )
                }
            }
            await ctx.author.send(embed=embed_generator(accepted_payload))
        else:
            denied_payload = {
                "body": {
                    "User denied team invite": (
                        f"'{user.name}' denied the invite you sent them to '{team_name}'!"
                    )
                }
            }
            await ctx.author.send(embed=embed_generator(denied_payload))

    @commands.command(name="disband", description="Disbands a created team")
    async def disband_command(self, ctx, team_name):
        found_team = await self._team_get(ctx, team_name)

        found_team.is_deleted = True
        self.session.commit()

        payload = {
            "body": {
                "Disbanded team": f"'{team_name}' has been disbanded successfully!"
            }
        }
        await ctx.send(embed=embed_generator(payload))

    @commands.command(
        name="remove", description="Removed mentioned user from the specified team"
    )
    async def remove_command(self, ctx, team_name, user: discord.Member):
        found_team = await self._team_get(ctx, team_name)
        found_user = dynamic_user_get(user.id, self.session)

        if found_team.is_deleted:
            payload = {
                "body": {
                    "No teams found": (
                        "Could not find any active"
                        f"teams by the name of '{team_name}'!"
                    )
                }
            }
        elif found_user in found_team.users:
            found_team.users.remove(found_user)
            self.session.commit()

            payload = {
                "body": {
                    "Removed user successfully": (
                        f"<@{user.id}> has been successfully removed from '{found_team.name}'!"
                    )
                }
            }
        else:
            payload = {
                "body": {
                    "User not in team": f"<@{user.id}> has not joined '{found_team.name}'!"
                }
            }

        await ctx.send(embed=embed_generator(payload))


def setup(client):
    """
    Binds this cog to the client object
    """

    client.add_cog(TeamLead(client))
    load_message("TeamLead")
