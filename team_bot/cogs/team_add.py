import re
from discord.ext import commands
from team_bot import Session
from team_bot.models import TeamModel, UserModel, dynamic_user_get
from team_bot.utils import Config, embed_generator, load_message


class TeamAdd(commands.Cog):
    """
    Adds a new team to the database.
    """

    def __init__(self, client):
        self.session = Session()
        self.client = client
        self.config = Config()

    @commands.command(name="newteam", description="Creates a new team")
    async def newteam_command(self, ctx, *, team_name):
        # If user is not the proper manager role
        if self.config.TEAM_MANAGER not in [roles.id for roles in ctx.author.roles]:
            unauthorized_payload = {
                "body": {
                    "Unauthorized": (
                        f"You are not the <@&{self.config.TEAM_MANAGER}> role "
                        "and therefore do not have access to this command!"
                    )
                }
            }
            await ctx.send(embed=embed_generator(unauthorized_payload))
            return

        def mention_check(message):
            """
            Checks to see if a message mentions a user
            """

            return message.author == ctx.message.author and message.mentions

        def logo_check(message):
            """
            Checks if a message contains a valid link
            """

            return message.author == ctx.message.author and message.attachments

        # --- Get leader --- #
        welcome_payload = {
            "body": {
                "Team creator": (
                    "Welcome to the team creator! You are currently"
                    f"creating a new team named '{team_name}'."
                ),
                "Leader name": "Please @ the leader for this new team.",
            }
        }

        await ctx.send(embed=embed_generator(welcome_payload))

        team_leader_message = await self.client.wait_for(
            "message", timeout=120, check=mention_check
        )  # Extra context for who was mentioned, temp var
        team_leader = team_leader_message.mentions[0]

        # --- Get logo --- #
        logo_payload = {"body": {"Logo": "Please enter a valid url to a logo."}}

        await ctx.send(embed=embed_generator(logo_payload))

        team_logo_image = await self.client.wait_for(
            "message", timeout=120
        )  # Extra context for the logo url, temp var

        # TODO add blocker for 0.1s so all 4 awaits don't trigger

        # --- Get initial team members --- #
        team_members_payload = {
            "body": {
                "Initial team members": (
                    "Please mention multiple users to"
                    "include them in your team by default"
                )
            }
        }
        await ctx.send(embed=embed_generator(team_members_payload))

        team_members = await self.client.wait_for(
            "message", timeout=120, check=mention_check
        )

        # --- Create Team --- #
        new_team = TeamModel(team_name, team_logo_image.attachments[0].url)
        owner_user = dynamic_user_get(team_leader.id, self.session)

        self.session.add_all(
            [owner_user, new_team]
        )  # Needs to be above so `dynamic_user_get` works

        member_list = []  # Stores ids of initial members given
        for member in team_members.mentions:
            member_list.append(dynamic_user_get(member.id, self.session))

        new_team.users.extend(member_list)
        new_team.owner = owner_user.discord_id

        self.session.commit()

        # --- Success message --- #
        success_payload = {
            "body": {
                "Team added successfully": (
                    f"Congratulations, '{team_name}'" "has been added successfully!"
                )
            }
        }
        await ctx.send(embed=embed_generator(success_payload))


def setup(client):
    """
    Binds this cog to the client object
    """

    client.add_cog(TeamAdd(client))
    load_message("TeamAdd")
