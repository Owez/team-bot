from discord.ext import commands
from team_bot import Session
from team_bot.models import TeamModel
from team_bot.utils import embed_generator, load_message


class TeamInfo(commands.Cog):
    """
    In-depth look at a specific team, similar to the TeamList cog
    but a smaller scope.
    """

    def __init__(self, client):
        self.session = Session()
        self.client = client

    @commands.command(
        name="info",
        description="Gives infomation about a given team, similar to `!listteam`.",
    )
    async def info_command(self, ctx, *, team_name):
        found_teams = self.session.query(TeamModel).filter_by(name=team_name)

        team_tally = 0

        for team in found_teams:
            if team.is_deleted:
                continue

            team_tally += 1

            member_list = "\n".join(
                [f"<@{member.discord_id}>" for member in team.users]
            )  # Makes the list of id's joined into a string `[@user][\n][@user]`

            payload = {
                "body": {
                    "Team": f"**{team.name}**, *slug: `{team.slug}`*",
                    "Owner": f"<@{team.owner}>",
                },
                "image": team.logo,
            }

            if not member_list:
                payload["body"]["Members"] = "No members"
            else:
                payload["body"]["Members"] = member_list

            try:
                await ctx.send(embed=embed_generator(payload))
            except:  # Remove image and try again
                payload.pop("image")
                await ctx.send(embed=embed_generator(payload))

        if team_tally == 0:
            empty_payload = {
                "body": {
                    "No teams found": (
                        f"Could not find any active teams by the name of '{team_name}'!"
                    )
                }
            }
            await ctx.send(embed=embed_generator(empty_payload))


def setup(client):
    """
    Binds this cog to the client object
    """

    client.add_cog(TeamInfo(client))
    load_message("TeamInfo")
