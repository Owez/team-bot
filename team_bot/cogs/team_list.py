from discord.ext import commands
from team_bot import Session
from team_bot.models import TeamModel
from team_bot.utils import embed_generator, load_message


class TeamList(commands.Cog):
    """
    A quick overview of all teams.
    """

    def __init__(self, client):
        self.session = Session()
        self.client = client

    @commands.command(name="listteam", description="Lists all of the active teams")
    async def listteam_command(self, ctx):
        payload = {"body": {}}

        for team in self.session.query(TeamModel).all():
            if team.is_deleted is False:
                payload["body"][f"Team '{team.name}'"] = f"Owner: <@{team.owner}>"

        if payload["body"]:
            await ctx.send(embed=embed_generator(payload))
        else:
            await ctx.send(
                embed=embed_generator(
                    {"body": {"No teams": "There are no active teams on record!"}}
                )
            )


def setup(client):
    """
    Binds this cog to the client object
    """

    client.add_cog(TeamList(client))
    load_message("TeamList")
