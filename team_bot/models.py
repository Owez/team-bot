import slugify
from sqlalchemy import Column, String, Integer, BigInteger, ForeignKey, Table, Boolean
from sqlalchemy.orm import relationship
from team_bot import Base, engine

association_table = Table(
    "association",
    Base.metadata,
    Column("user_discord_id", BigInteger, ForeignKey("users.discord_id")),
    Column("team_id", Integer, ForeignKey("teams.id")),
)


class UserModel(Base):
    __tablename__ = "users"

    discord_id = Column(BigInteger, primary_key=True)
    owned_teams = relationship("TeamModel")
    teams = relationship(
        "TeamModel", secondary=association_table, back_populates="users"
    )


class TeamModel(Base):
    __tablename__ = "teams"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    slug = Column(String)
    logo = Column(String)
    owner = Column(BigInteger, ForeignKey("users.discord_id"))
    users = relationship(
        "UserModel", secondary=association_table, back_populates="teams"
    )
    is_deleted = Column(Boolean)

    def __init__(self, name, logo):
        self.name = name
        self.slug = slugify.slugify(name)
        self.logo = logo
        self.is_deleted = False


def dynamic_user_get(user_id, db_session):
    """
    Creates a user in the database if they are not
    already in it and returns a user object no matter
    """

    found_user = db_session.query(UserModel).get(user_id)

    if found_user is None:
        found_user = UserModel(discord_id=user_id)

    return found_user


Base.metadata.create_all(engine)
