# Team Bot

## About

"Team Bot" is a discord.py-based bot that manages teams.

## Setup

1. Install Python 3.6 and the [`pipenv` package](https://docs.pipenv.org/en/latest/).
2. Enter the directory where this `README.md` is located.
3. Run in the terminal `pipenv install` to install all of the dependencies for this project and create a virtual enviroment to safely run inside.
4. Enter the virtual environment with `pipenv shell`.
5. Once you are inside of the virtual environment, you can set the bot's token using an environment variable. On Linux, this would look like: `export TOKEN=[bot token]` and on Windows, it would look something like: `set TOKEN=[bot token]`.
6. "Copy ID" of the `Tourney Manager` role and add it to the `tourney_manager` value of the `config.toml` file situated next to this.
7. Run the bot with `python app.py`. Please note that you may have to change `python` to `py` if you are running on some Windows machines.

## Commands

- `!newteam [team name]` Starts an interactive session for any management user to create a new team. Only avalible to the managment role
- `!disband [team name]` Deletes a team. Only avalible to team leaders
- `!invite @user` Invites a user (`@user`) to the team. Will pm the team leader if it's accepted or not. Only avalible to team leaders.
- `!info [team name]` Gives infomation on a specific team
- `!listteam` Gives a large list of all teams
- `!remove [team name] @user` Removes a user (`@user`) from the given team. Only avalible to team leaders.
- `!ping` A testing ping command to see if the bot is running correctly.

## Developer

- [Owez](https://ogriffiths.com/)
